/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */

package eu.paasage.upperware.solvertodeployment.api;

import eu.paasage.upperware.loadPaaSageInstance.ModelProcess;

public interface ILoaderFactory {

	public ModelProcess loadModel(String path); 

}
